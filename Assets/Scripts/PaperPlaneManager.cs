using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaperPlaneManager : MonoBehaviour
{
    public GameObject paperPlaneStart;
    public GameObject paperPlane1;
    public GameObject paperPlane2;
    public GameObject paperPlane3;
    public GameObject paperPlane4;
    public GameObject paperPlane5;

    int paperPlanePhase = 0;

    void Start()
    {
        EventManager.StartListening(Events.Success, paperPlaneAnimator);
        EventManager.StartListening(Events.InitiateFolding, Initiate);
    }

    private void Initiate()
    {
        paperPlanePhase = 0;

        paperPlaneStart.SetActive(true);
        paperPlane1.SetActive(false);
        paperPlane2.SetActive(false);
        paperPlane3.SetActive(false);
        paperPlane4.SetActive(false);
        paperPlane5.SetActive(false);
    }

    private void paperPlaneAnimator()
    {
        paperPlanePhase++;

        if (paperPlanePhase == 1)
        {
            paperPlaneStart.SetActive(false);
            paperPlane1.SetActive(true);
        }

        if (paperPlanePhase == 2)
        {
            paperPlane1.SetActive(false);
            paperPlane2.SetActive(true);
        }

        if (paperPlanePhase == 3)
        {
            paperPlane2.SetActive(false);
            paperPlane3.SetActive(true);
        }

        if (paperPlanePhase == 4)
        {
            paperPlane3.SetActive(false);
            paperPlane4.SetActive(true);
        }

        if (paperPlanePhase == 5)
        {
            paperPlane4.SetActive(false);
            paperPlane5.SetActive(true);
        }
    }
}
