using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject anchor;
    public Vector3 playerPosition;

    private void Start()
    {
        StartCoroutine("Follower");
    }

    void Update()
    {

        if (Input.GetButtonDown("Initiate"))
        {
            EventManager.TriggerEvent(Events.InitiateFolding);
            //player.SetActive(false);
            StopCoroutine("Follower");
        }

        if (Input.GetButtonDown("Cancel"))
        {
            EventManager.TriggerEvent(Events.EndFolding);
            StartCoroutine("Follower");
        }

    }

    IEnumerator Follower()
    {
        playerPosition = GameObject.Find("Player").transform.position;
        anchor.transform.position = playerPosition;
        yield return true;
    }
}
