using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mesh_Controller : MonoBehaviour
{
    public GameObject paper1;
    public GameObject paper2;
    public GameObject paper3;
    public GameObject paper4;
    public GameObject paper5;
    

    // Start is called before the first frame update
    void Start()
    {
        paper1.SetActive(true);
        paper2.SetActive(false);
        paper3.SetActive(false);
        paper4.SetActive(false);
        paper5.SetActive(false);
      
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            paper1.SetActive(false);
            paper2.SetActive(true);

        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            paper2.SetActive(false);
            paper3.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            paper3.SetActive(false);
            paper4.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            paper4.SetActive(false);
            paper5.SetActive(true);
        }

       
      
    }
}
